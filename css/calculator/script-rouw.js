$(document).ready(function(){
	var matched, browser;

	jQuery.uaMatch = function( ua ) {
		ua = ua.toLowerCase();

		var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
			/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
			/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
			/(msie) ([\w.]+)/.exec( ua ) ||
			ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
			[];

		return {
			browser: match[ 1 ] || "",
			version: match[ 2 ] || "0"
		};
	};

	matched = jQuery.uaMatch( navigator.userAgent );
	browser = {};

	if ( matched.browser ) {
		browser[ matched.browser ] = true;
		browser.version = matched.version;
	}

	// Chrome is Webkit, but Webkit is also Safari.
	if ( browser.chrome ) {
		browser.webkit = true;
	} else if ( browser.webkit ) {
		browser.safari = true;
	}

	jQuery.browser = browser;


	if (!$.browser.opera) {
		// select element styling
		$('#calculator select').each(function(){
			var title = $(this).attr('title');
			if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
			$(this)
				.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
				.after('<span class="select">' + title + '</span>')
				.change(function(){
					val = $('option:selected',this).text();
					$(this).next().text(val);
					});
		});
	}

	$('#type').parent().find('.select').html('Dubbel vierkant');
	$('#size').parent().find('.select').html('13x13 cm');
	$('#papertypes').parent().find('.select').html('Natuurkarton');
	$('#enveloppen').parent().find('.select').html('Wit, gekleurd of kraft');

	$('#type').change(function(){
		var type = get_type();
		var html = '';
		switch (type) {
			case 'enkel vierkant':
				/*html += '<option value="extra klein">8x8 cm</option>';*/
				html += '<option value="klein">11x11 cm</option>';
				html += '<option value="normaal" selected="selected">13x13 cm</option>';
				html += '<option value="groot">15x15 cm</option>';
				document.getElementById('size').innerHTML = html;
				$('#size').next('.select').html('13x13 cm');
				break;

			case 'enkel staand':
				/*html += '<option value="klein">5,4x8,1 cm</option>';
				html += '<option value="postkaart">10x15 cm</option>';*/
				html += '<option value="normaal" selected="selected">11,4x17,1 cm</option>';
				html += '<option value="groot">14,4x21,6 cm</option>';
				document.getElementById('size').innerHTML = html;
				$('#size').next('.select').html('11,4x17,1 cm');
				break;

			case 'enkel liggend':
				/*html += '<option value="klein">8,1x5,4 cm</option>';*/
				html += '<option value="postkaart" selected="selected">15x10 cm</option>';
				html += '<option value="normaal">17,1x11,4 cm</option>';
				html += '<option value="groot">21,6x14,4 cm</option>';
				document.getElementById('size').innerHTML = html;
				$('#size').next('.select').html('15x10 cm');
				break;

			case 'dubbel vierkant':
				html += '<option value="klein">11x11 cm</option>';
				html += '<option value="normaal" selected="selected">13x13 cm</option>';
				html += '<option value="groot">15x15 cm</option>';
				/*html += '<option value="extra groot">21x21 cm</option>';*/
				document.getElementById('size').innerHTML = html;
				$('#size').next('.select').html('13x13 cm');
				break;

			case 'dubbel staand':
				/*html += '<option value="postkaart">10x15 cm</option>';*/
				html += '<option value="normaal" selected="selected">11,4x17,1 cm</option>';
				/*html += '<option value="groot">14,4x21,6 cm</option>';
				html += '<option value="extra groot">20x30 cm</option>';*/
				document.getElementById('size').innerHTML = html;
				$('#size').next('.select').html('11,4x17,1 cm');
				break;

			case 'dubbel liggend':
				html += '<option value="postkaart" selected="selected">15x10 cm</option>';
				html += '<option value="normaal">17,1x11,4 cm</option>';
				/*html += '<option value="groot">21,6x14,4 cm</option>';
				html += '<option value="extra groot">30x20 cm</option>';*/
				document.getElementById('size').innerHTML = html;
				$('#size').next('.select').html('15x10 cm');
				break;
		}
	});
});

	// get type
	function get_type() {
		var type = 'enkel';
		type = document.getElementById("type");
		return type.options[type.selectedIndex].value;
	}

	// function radio checked
	function check(arg) {
		var a = arg.split('|');
		for (i=0;i<a.length;i++) {
			if (document.getElementById(a[i]).onchange)
				return document.getElementById(a[i]).value;
		}
	}

	function placeHolder(element){
		var standard_message = $(element).val();
		$(element).focus(
			function() {
				if ($(this).val() == standard_message)
					$(this).val("");
			}
		);
		$(element).blur(
			function() {
				if ($(this).val() == "")
					$(this).val(standard_message);
			}
		);
	}

	function posAmountCard(n) {
		if (n==1) {
			return 4;
		} else if (n>1 && n<10) {
			return 5;
		} else if (n>9 && n<50) {
			return 6;
		} else if (n>49 && n<100) {
			return 7;
		} else if (n>99 && n<200) {
			return 8;
		} else if (n>199) {
			return 9;
		}
	}


	function posPaperType(s) {
		switch (s) {
			case 'parelmoer': return 13; break;
			case 'ivory-gold': return 13; break;
			case 'oud-hollands': return 14; break;
			case 'silk-mc': return 15; break;
			case 'fotokaart': return 16; break;
			case 'natuurkarton': return 17; break;
		}
	}

	function posEnvelopType(s) {
		switch (s) {
			case 'enveloppen': return 10; break;
			case 'Envelop goud/zilver': return 11; break;
		}
	}

	function posShip(n) {
		if (n==1) {
			return 16;
		} else if (n>1 && n<20) {
			return 17;
		} else if (n>19) {
			return 18;
		}
	}


function calprice(s)
	{
		jQuery('#message p').remove();
		var num_card = jQuery('#amount-card').val();
		var num_envelop = jQuery('#amount-envelop').val();

		var size = jQuery('#size').parent().find('.select').html();
		switch(size) {
			case '8x8 cm': size = 'extra klein'; break;
			case '10x15 cm': size = 'postkaart'; break;
			case '11x11 cm': size = 'klein'; break;
			case '13x13 cm': size = 'normaal'; break;
			case '15x15 cm': size = 'groot'; break;
			case '21x21 cm': size = 'extra groot'; break;
			case '5,4x8,1 cm': size = 'klein 1'; break;
			case '11,4x17,1 cm': size = 'normaal 1'; break;
			case '14,4x21,6 cm': size = 'groot 1'; break;
			case '20x30 cm': size = 'extra groot 1'; break;
			case '8,1x5,4 cm': size = 'klein 2'; break;
			case '15x10 cm': size = 'postkaart 2'; break;
			case '17,1x11,4 cm': size = 'normaal 2'; break;
			case '21,6x14,4 cm': size = 'groot 2'; break;
			case '30x20 cm': size = 'extra groot 2'; break;
			case '13,5x12 cm': size = 'normaal 3'; break;
			case '39x13 cm': size = 'groot 3'; break;
			case '21x10 cm': size = 'normaal 4'; break;
			case '30x10 cm': size = 'normaal 5'; break;
		}

		var papertype = jQuery('#papertypes').parent().find('.select').html();
		switch(papertype) {
			case 'Parelmoer': papertype = 'parelmoer'; break;
			case 'Ivory gold': papertype = 'ivory-gold'; break;
			case 'Oud-hollands': papertype = 'oud-hollands'; break;
			case 'Silk MC': papertype = 'silk-mc'; break;
			case 'Fotokaart': papertype = 'fotokaart'; break;
			case 'Natuurkarton': papertype = 'natuurkarton'; break;
		}

		var enveloptype = jQuery('#enveloppen').parent().find('.select').html();
		switch(enveloptype) {
			case 'Wit, gekleurd of kraft': envelop = 'enveloppen'; break;
			case 'Goud of zilver': envelop = 'Envelop goud/zilver'; break;
		}

		var type = jQuery('#type').parent().find('.select').html();
		switch(type) {
			case 'Enkel vierkant': type = 'single square'; break;
			case 'Enkel staand': type = 'single portrait'; break;
			case 'Enkel liggend': type = 'single landscape'; break;
			case 'Dubbel vierkant': type = 'double square'; break;
			case 'Dubbel staand': type = 'double portrait'; break;
			case 'Dubbel liggend': type = 'double landscape'; break;
		}

		var message = true;
		var data_arr = [];

		if (s == "first") {
			type = 'double square';
			size = 'normaal';
			papertype = 'natuurkarton';
			envelop = 'enveloppen';
			num_card = 50;
			num_envelop = 50;
		}

		if (s == 'last') {
			if (size == '') { jQuery('#message').show(); jQuery('#message').text('not niet alle velden zijn ingevuld...'); message = false; }
			if (papertype == '') { jQuery('#message').show(); jQuery('#message').text('not niet alle velden zijn ingevuld...'); message = false; }
			if (num_card == '') { jQuery('#message').show(); jQuery('#message').text('not niet alle velden zijn ingevuld...'); message = false; }
		}

		if (message) {
			jQuery('#message').css('display','none');
			jQuery.get('/css/calculator/pricesheet-rouw-hipdesign.csv', function(data) {
				data = data.split(',end');

				for (i=0;i<data.length;i++) {
					data_arr[i] = data[i].split(',');
				}

				var group01 = type + ',' + 'card' + ',' + size;
				for (j=0;j<data.length;j++) {
					if (data[j].indexOf(group01) != -1) {
						var pos01 = j;
						break;
					}
				}

				if (num_card >0) {
					var pos02 = posAmountCard(num_card);
					var pos03 = posPaperType(papertype);
					var pos05 = posEnvelopType(envelop);
					//var pos04 = posShip(num_card);
					if (num_envelop == '') num_envelop = num_card;

					var amount_card = num_card*data_arr[pos01][pos02];
					var price_papertype = num_card*data_arr[pos01][pos03];
					var price_envelop = num_envelop*data_arr[pos01][pos05];
					//var ship = 1*data_arr[pos01][pos04];
					var price = amount_card + price_papertype + price_envelop;

					jQuery('#result input').val(price.toFixed(2));
				} else {
					jQuery('#result input').val(0);
				}
			});

		}
	}

jQuery(function(){
	calprice('first');
	jQuery('#type').change(function(e){
       if($("#amount-envelop").val() >= 0 || $("#amount-card").val() >= 0){
			calprice('last');
		}
    });
	jQuery('#size').change(function(e){
       if($("#amount-envelop").val() >= 0 || $("#amount-card").val() >= 0){
			calprice('last');
		}
    });
	jQuery('#enveloppen').change(function(e){
       if($("#amount-envelop").val() >= 0 || $("#amount-card").val() >= 0){
			calprice('last');
		}
    });
	jQuery('#papertypes').change(function(e){
       if($("#amount-envelop").val() >= 0 || $("#amount-card").val() >= 0){
			calprice();
		}
    });

	$('#amount-card').keyup(function () {
		calprice('last');
	});
	$('#amount-envelop').keyup(function () {
		calprice('last');
	});

	$(".amount input[type='text']").each(function(index, element) {
		  placeHolder($(this));
	});


	$('#amount-card, #amount-envelop').keydown(function(event) {
		// Allow special chars + arrows
		if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9
			|| event.keyCode == 27 || event.keyCode == 13
			|| (event.keyCode == 65 && event.ctrlKey === true)
			|| (event.keyCode >= 35 && event.keyCode <= 39)){
				return;
		}else {
			// If it's not a number stop the keypress
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault();
			}
		}
	});

});



$(document).ready( function() {
    //$("#total").val("0");
    $("#amount").val("50");
    $("#amountb").val("50");
    $( function() {


        var var_slider = $("<div id='slider'></div>").insertAfter($("#amount-card")).slider({
            value: $('#amount-card').val(),
           	min: 0,
            max: 300,
			range: "min",
            step: 1,
            slide: function(event, ui) {
				var label = '#amount-card';
				//$(label).val(ui.value);
				$(label).val(ui.value).position({
					my: 'left top',
					at: 'left top',
					of: ui.handle,
					offset: "-36, -17"
				});
				calprice('last');
            }
        });

        var var_sliderb = $("<div id='sliderb'></div>").insertAfter($("#amount-envelop")).slider({
            value: $('#amount-envelop').val(),
            min: 0,
            max: 300,
			range: "min",
            step: 1,
            slide: function(event, ui) {
                var label = '#amount-envelop';
				//$(label).val(ui.value).position();
				$(label).val(ui.value).position({
					my: 'left top',
					at: 'left top',
					of: ui.handle,
					offset: "-36, -17"
				});
                calprice('last');
			}
        });

		/*$('#amount-card').val($('#slider').slider('values', 50)).position({
			my: 'center top',
			at: 'center bottom',
			of: $('#slider a:eq(0)'),
			offset: "75, -34"
		});

		$('#amount-envelop').val($('#sliderb').slider('values', 50)).position({
			my: 'center top',
			at: 'center bottom',
			of: $('#sliderb a:eq(0)'),
			offset: "75, -34"
		});*/

		$('#amount-card').change(function(){
			var_slider.slider('value',$(this).val());
			$(this).position({
				my: 'left top',
				at: 'left top',
				of: '#slider .ui-slider-handle',
				offset: "-36, -17"
			});
		});
		$('#amount-envelop').change(function(){
			var_sliderb.slider('value',$(this).val());
			$(this).position({
				my: 'left top',
				at: 'left top',
				of: '#sliderb .ui-slider-handle',
				offset: "-36, -17"
			});
		});

    });

});
function get_url_parameter( param ){
    param = param.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var r1 = "[\\?&]"+param+"=([^&#]*)";
    var r2 = new RegExp( r1 );
    var r3 = r2.exec( window.location.href );
    if( r3 == null ) return ""; else return decodeURIComponent(r3[1]);
}

if (get_url_parameter('url') != '') {
    var html='<img class="xsm-5" src="' + get_url_parameter('src1') + '" alt=""><img class="xsm-7" src="' + get_url_parameter('src2') + '" alt="">';
    html += '<span><a href="javascript:;" class="zoom">Vergroot kaartje</a></span>';
    document.getElementById('img-preview').innerHTML = html;
    document.getElementById("input_12").value = get_url_parameter('url');
}

if (get_url_parameter('name') != '') {
    var html='<img  src="/design_larger_thumb/' + get_url_parameter('name') + '" alt="' + get_url_parameter('name') + '">';
    document.getElementById('img-preview').innerHTML = html;
    document.getElementById("input_12").value = "http://hipdesign.nl/" + get_url_parameter('main_cat') + "/" + get_url_parameter('cat') + "/" + get_url_parameter('name');
}

document.getElementById("si" + "mple" + "_spc").value = "42521541882352-42521541882352";
document.getElementById("input_3").onchange = function(){
    var value = document.getElementById("input_3").value;
    switch (value) {
        case 'Enkel vierkant':
            var html = '<option value="8x8 cm"> 8x8 cm</option>';
            html += '<option value="11x11 cm"> 11x11 cm</option>';
            html += '<option value="13x13 cm"> 13x13 cm</option>';
            html += '<option value="15x15 cm"> 15x15 cm</option>';
            document.getElementById("input_4").innerHTML = html;
            break;
        case 'Rechthoek staand':
            var html = '<option value="10x15 cm"> 10x15 cm</option>';
            html += '<option value="11x17 cm"> 11x17 cm</option>';
            html += '<option value="14x21 cm"> 14x21 cm</option>';
            document.getElementById("input_4").innerHTML = html;
            break;
        case 'Rechthoek liggend':
            var html = '<option value="15x10 cm"> 15x10 cm</option>';
            html += '<option value="17x11 cm"> 17x11 cm</option>';
            html += '<option value="21x14 cm"> 21x14 cm</option>';
            document.getElementById("input_4").innerHTML = html;
            break;
        case 'Dubbel vierkant':
            var html = '<option value="11x11 cm"> 11x11 cm</option>';
            html += '<option value="13x13 cm" selected="selected"> 13x13 cm</option>';
            html += '<option value="15x15 cm"> 15x15 cm</option>';
            document.getElementById("input_4").innerHTML = html;
            break;
        case 'Enkel staand':
            var html = '<option value="11x17 cm"> 11x17 cm</option>';
            html += '<option value="14x21 cm"> 14x21 cm</option>';
            document.getElementById("input_4").innerHTML = html;
            break;
        case 'Enkel liggend':
            var html = '<option value="17x11 cm"> 17x11 cm</option>';
            html += '<option value="21x14 cm"> 21x14 cm</option>';
            document.getElementById("input_4").innerHTML = html;
            break;
        case 'Rechthoek liggend (met kortere flap)':
            var html = '<option value="17x11 cm"> 17x11 cm (uitgeklapt 28x11 cm met flap van 4 cm)</option>';
            document.getElementById("input_4").innerHTML = html;
            break;
        case 'Drieluik':
            var html = '<option value="10x10x10 cm"> 10x10x10 cm</option>';
            html += '<option value="13x13x13 cm"> 13x13x13 cm</option>';
            document.getElementById("input_4").innerHTML = html;
            break;
        default : break;
    }
};
$(function () {
    carousel = (function(){
	    var box = document.querySelector('.customer-experiences');
	    var next = box.querySelector('.next');
	    var prev = box.querySelector('.prev');
	    var items = box.querySelectorAll('.content li');
	    var counter = 0;
	    var amount = items.length;
	    var current = items[0];
	    box.classList.add('active');
	    function navigate(direction) {
		    current.classList.remove('current');
		    counter = counter + direction;
		    if (direction === -1 && counter < 0) {
		        counter = amount - 1;
            }
		    if (direction === 1 && !items[counter]) {
		        counter = 0;
		    }
		    current = items[counter];
		    current.classList.add('current');
	    }
	    next.addEventListener('click', function(ev) {
		    navigate(1);
	    });
	    prev.addEventListener('click', function(ev) {
		    navigate(-1);
	    });
	        navigate(0);
	    })();

        if ($(window).width() < 480) {
            var menu = $('menu ul');
            $('.collapse').prepend(menu);
        }

        if($(window).width() > 481 && $(window).width() < 991) {
            $(window).scroll(function () {
                if ($(document).scrollTop() > 750) {
                    $('menu').css({'position': 'fixed', 'top': '0', 'box-shadow': '1px 2px 3px #ddd', 'margin-bottom': '50px;'});
					$('#customizing #formats').css({'padding-top': '230px'});
					$('#customizing #experiences').css({'padding-top': '60px'});
					$('#customizing #tips-creating').css({'padding-top': '50px'});
					$('#customizing #tips-packaging').css({'padding-top': '70px'});
					$('#customizing #inspiration').css({'padding-top': '100px'});
                } else {
                    $('menu').css({'position': 'relative', 'top': '0'});
					$('#customizing #formats').css({'padding-top': '70px'});
					$('#customizing #experiences').css({'padding-top': '50px'});
					$('#customizing #tips-creating').css({'padding-top': '0px'});
					$('#customizing #tips-packaging').css({'padding-top': '0px'});
					$('#customizing #inspiration').css({'padding-top': '40px'});
                }
            });
        };

		if($(window).width() > 992) {
            $(window).scroll(function () {
                if ($(document).scrollTop() > 680) {
					$('menu').css({'position': 'fixed', 'top': '0', 'box-shadow': '1px 2px 3px #ddd'});
					$('#customizing').css({'padding-top': '60px'});
                } else {
                    $('menu').css({'position': 'relative', 'top': '0', 'box-shadow': 'none'});
					$('#customizing').css({'padding-top': '0'});
                }
            });
        };

		/*if($(window).width() > 992) {
            $(window).scroll(function () {
                if ($(document).scrollTop() > 0) {
					$('menu').css({'position': 'absolute'});
					$('menu').animate({top: '0px'}, 2000);
					$('menu').css({'position': 'fixed', 'top': '0'});
				};
            });
        };*/

	/*if($(window).width() > 992) {
        $(window).scroll(function () {
            if ($(document).scrollTop() > 500) {
				$('menu').css({'position': 'absolute'});
				$('menu').animate({top: '0'}, 500, function() {
					$('menu').css({'position': 'fixed', 'top': '0'});
				});
			};
        });
	};*/

	$("menu ul li a").on('click', function(event) {
		$(this).addClass('active');
		var li = $(this).parent('li');
		var menuitem = li.siblings('li');
		menuitem.each(function(){
			if($(this).children('a').hasClass('active')){
				$(this).children('a').removeClass('active');
			};
		});

		if (this.hash !== "") {
			event.preventDefault();
			var hash = this.hash;
			$('html, body').animate({
				scrollTop: $(hash).offset().top-50
			}, 800, function(){
				window.location.hash = hash;
			});
		}
  });
});

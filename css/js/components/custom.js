(function($) {
    "use strict";
    var crumbs = $('#crumbs');
    $('#mid').prepend(crumbs);

    $('a.zoek').click(function(e) {
        e.preventDefault();
        $(this).toggleClass('active').parent().find('form input').fadeToggle(function() {
            $('.sForm form input').focus();
        });
    });

    // stuff in header
    var $header = $('#header');
    var $mid = $('#mid');
    var $sidebar = $mid.find('#sidebar');

    var $pagecontent = $mid.find('#pagecontent');
    var $headerMenu = $header.find('.topbar-menu-right');

    //custom voor design pagina
    var pathname = window.location.pathname;
    var pathname_a = pathname.split("/");


    $(window).load(function() {
        if ($('#body_contact').length > 0) {
            $('#body_contact').find('.onze').before($('.contactfrm'));
        }
    });

    $headerMenu.find('#navbar-basket').html($("#headerbasket #basket_count").html());
    if ($("#headerlogout").length > 0) {
        var logout = "<a href='/logout' title='Uitloggen' class='logout'><span>" + $("#headerlogout span").html() + "</span></a>";
    }
    if (!$("#headerlogin").length) {
        $headerMenu.find('#navbar-login').remove();
    }

    if (!$("#headerlogout").length) {
        $headerMenu.find('#navbar-logout').remove();
    }

    // append visual content
    if (!$header.find('#visual-content').length) {
        $header.append('<div id="visual-content"></div>');
    }

    $pagecontent.find('.visual').appendTo($header.find('#visual-content')); // set visual to visual content in header

    // check type of page should be added to first dive
    // template-type
    // has class fluid-template
    // has class sidebar-template
    if ($pagecontent.find('#template-fluid').length) {
        $pagecontent.removeClass("sm-9 md-9").addClass("sm-12 md-12");
        $sidebar.hide();
        $('body').addClass('body-template-fluid');
    } else if ($pagecontent.find('#template-sidebar').length) {
        $pagecontent.removeClass("sm-9 md-12").addClass("sm-9 md-9");
    } else {
        $pagecontent.removeClass("sm-9 md-12").addClass("sm-9 md-9");
    }

    if ($('#body_design').length) {
        $('#categories').remove();
        $('#pagecontent').removeClass('md-9').addClass('md-12');
    }

    // set the menu's in sidebar. plaats in een pagina een <div data-sidebar="menu1|menu2|menut3" en in het menu de attr die je wilt laten zien <div id="menu1">
    var $dataSidebar = $pagecontent.find('[data-sidebar]');

    if ($dataSidebar.length) {
        $dataSidebar.data('sidebar').split("|").map(function(response) {
            if ($sidebar.find('#' + response).length) {
                var $menuItem = $sidebar.find('#' + response);
                //alert($menuItem);
                $menuItem.show();
            }
        });

    } else {
        $sidebar.find('#service').show();
        $sidebar.find('#photo').show();
    }

    // zet het filter menu in een accordion
    var $filterCards = $sidebar.find('#filter_cards');
    var $filterMenu = $sidebar.find('#filter_menu');
    if ($filterMenu.children().length > 0) {
        $filterCards.addClass("accordion");
        $filterMenu.addClass("accordion-body show");
        $filterCards.prepend("<a class='accordion-button active' href='#'><span>Filters</span></a>");
        var $prijzenMenu = $sidebar.find('.menu-prijzen').last();
        if ($prijzenMenu.length) {
            // $prijzenMenu.after($filterCards);
            $filterCards.appendTo($('#sidebar'));
        }
    }

    //Tablet filter functionaliteiten
    if ($(window).width() <= 820) {
        if ($('.filCTA').length && $('.filCTA').is(':visible')) {
            $('#filter_cards').prependTo('body').removeClass('accordion').addClass('mobFilter');
            $('#filter_cards').find('.accordion-button').hide();

            $('a.filCTA').on('click', function(e) {
                e.preventDefault();
                $('div.filCTAwrp').toggleClass('active');
                $('.mobFilter').toggleClass('active');
                $('.filRes em').html($('#card_previews>div').length);
                $('body').toggleClass('noflow');
            });

            $('li.tag-li').on('click', function() {
                setTimeout(function() {
                    $('.filRes em').html($('#card_previews>div').length);
                }, 500);
            });

            $('.filRes').on('click', function() {
                $('div.filCTAwrp').toggleClass('active');
                $('.mobFilter').toggleClass('active');
                $('body').toggleClass('noflow');
            });
        }
    }

    //hide side bar menu als scherm te klein wordt
    if ($(window).width() < 768) {
        if ($filterCards.length) {
            $filterMenu.removeClass("show");
            $filterCards.addClass("small");
            $sidebar.find(".accordion-button").removeClass("active");
            $pagecontent.find('form').before($filterCards);
        }
        $sidebar.hide();
    }

    //klasses toevoegen voor custom styling verschillende pagina's
    var loc = window.location.href; // returns the full URL
    var $menu = $header.find(".middlebar-menu-right");

    if (loc == "http://hipdesign.nl/trouwkaarten/save-the-date-kaarten") {
        $('body').addClass('blockbanner');
    };
    if (/samen/.test(loc)) {
        if (loc == "http://hipdesign.nl/samen-ontwerpen") {
            $('body').addClass('geboortekaartjes samen-page');
        }
        if (/ontwerp-samen/.test(loc)) {
            if (/geboortekaartjes/.test(loc)) {
                $('body').addClass('geboortekaartjes');
            } else if (/trouwkaarten/.test(loc)) {
                $('body').addClass('trouwkaarten');
            }
        } else {
            $('body').addClass('subpage samen');
        }
    } else if (/geboortekaartjes/.test(loc)) {
        $('body').addClass('geboortekaartjes');
        $menu.find("#geboortekaartjes").addClass('active').parent('li').addClass('open');
    } else if (/wenskaarten/.test(loc)) {
        $('body').addClass('wenskaarten');
        $menu.find("#wenskaarten").addClass('active');
    } else if (/trouwkaarten/.test(loc)) {
        $('body').addClass('trouwkaarten');
        $menu.find("#trouwkaarten").addClass('active');
    } else if (/zelf-temp/.test(loc)) {
        $('body').addClass('geboortekaartjes zelf');
        $menu.find("#geboortekaartjes").addClass('active');
    } else if (/uitnodiging-maken/.test(loc)) {
        $('body').addClass('uitnodiging-maken');
        $menu.find("#uitnodiging-maken").addClass('active');
    } else if (/Uitnodigingen-home/.test(loc)) {
        $('body').addClass('uitnodiging-maken');
        $menu.find("#uitnodiging-maken").addClass('active');
    } else if (/jubileumkaarten/.test(loc)) {
        $('body').addClass('jubileumkaarten');
        $menu.find("#jubileum-kaarten").addClass('active');
    } else if (/jubileumkaarten/.test(loc)) {
        $('body').addClass('uitnodiging-maken jubileumkaarten');
    } else if (/rouwkaarten/.test(loc)) {
        $('body').addClass('rouwkaarten');
        $menu.find("#rouwkaarten").addClass('active');
    } else if (/kraamborrelkaartje/.test(loc)) {
        $('body').addClass('subpage kraamborrelkaartje');
    } else if (/cadeaus/.test(loc)) {
        $('body').addClass('wenskaarten cadeaus');
    } else {
        $('body').addClass('subpage');
    }

    $('body.geboortekaartjes .middlebar .logo').find('img').attr('src', '/img/logo-geboorte.png');
    $('body.trouwkaarten .middlebar .logo').find('img').attr('src', '/img/logo-trouwen.png');
    $('body.uitnodiging-maken .middlebar .logo').find('img').attr('src', '/img/logo-uitnodigingen.png');
    $('body.rouwkaarten .middlebar .logo').find('img').attr('src', '/img/logo-rouw.png');
    $('body.wenskaarten .middlebar .logo').find('img').attr('src', '/img/logo-wenskaarten.png');
    $('body.jubileumkaarten .middlebar .logo').find('img').attr('src', '/img/logo-jubileumkaarten.png');

    $('body.geboortekaartjes .topbar a.logosmall').find('img').attr('src', '/img/logo-geboorte.png');
    $('body.trouwkaarten .topbar a.logosmall').find('img').attr('src', '/img/logo-trouwen.png');
    $('body.uitnodiging-maken a.logosmall .logo').find('img').attr('src', '/img/logo-uitnodigingen.png');
    $('body.rouwkaarten .topbar a.logosmall').find('img').attr('src', '/img/logo-rouw.png');
    $('body.wenskaarten .topbar a.logosmall').find('img').attr('src', '/img/logo-wenskaarten.png');
    $('body.jubileumkaarten .topbar a.logosmall').find('img').attr('src', '/img/logo-jubileumkaarten.png');

    //custom script voor info en prijzen. Open alleen de accordion van welke de link is ingedrukt
    var $prijzenlink = $sidebar.find('.menu-prijzen ul li a');
    $prijzenlink.on('click', function(e) {
        var h = $(this).attr('href');
        var h_a = h.split('#');
        if (h_a.length > 1) {
            var $id = $mid.find('#' + h_a[1]);
            var $prijsitem = $pagecontent.find('.accordion, .prijs');
            $prijsitem.each(function() {
                $(this).find('.accordion-body').removeClass("show");
            });
            $id.find('.accordion-body').addClass("show");

        }
    });

    //ook bij het laden van de pagina de goede accordion openen
    function openAcc() {
        if (location.hash) {
            var h_a = location.hash;
            var $id = $mid.find(h_a);
            var $prijsitem = $pagecontent.find('.accordion, .prijs');
            $prijsitem.each(function() {
                $(this).find('.accordion-body').removeClass("show");
            });
            $id.find('.accordion-body').addClass("show");

        }
    }
    openAcc();


    /*if (pathname_a[1] == 'wenskaarten') {
        $usps.text("PAKKET VOOR 18.00 BESTELD = DEZELFDE DAG VERZONDEN");
    }*/

    if ($('#body_design').length) {
        $sidebar.remove();
        $pagecontent.removeClass('sm-9');
        $('.proefDrukPrijs').remove();
        $(".detAccord.first, .accordion.first").next().remove();
        var $prijslink = "/" + pathname_a[1] + "/prijzen-info#prijzen";
        $("a.meerPrijsLink").prop('href', $prijslink);

        if ($(".desUsp").length) {

            if (pathname_a[1] == 'wenskaarten') {
                var $tekst = $(".desUsp").text();
                $usps.text($tekst);
                $breadcrumb.addClass("sm-8");
                $usps.addClass("sm-4");
                $usps.removeClass("sm-6");
                $breadcrumb.removeClass("sm-6");
            }
            // $(".desUsp").remove();
        }
        $('#card_previews_horizontal').parent().addClass('cardrail');
    }

    $('#body_design #prevWrap').before('<div id="card-detail-link" style="display:block"><a href="javascript:;" onclick="history.go(-1);return false;"><span>&lt;&lt;</span> <span class="txt">Ga terug</span></a></div>');
    if (pathname_a[1] != 'wenskaarten') {
        var loc = location.href;
        var src1 = $('#voor_preview img:eq(0)').attr('src');
        var src2 = $('#voor_preview img:eq(1)').attr('src');
        var $body_design = $('#body_design');

        if ($('div.box-footer').length) {
            $('#body_design .box-body').after('<form action="/samen" method="get" id="samenfrm"><input type="hidden" name="url" id="url" value="' + loc + '"><input type="hidden" id="src1" name="src1" value="' + src1 + '"><input type="hidden" id="src2" name="src2" value="' + src2 + '"></form><div class="extra"><span>Wil je wat extra&rsquo;s?</span><a href="javascript:;" id="samenbutton" rel="nofollow">Ontwerp dit kaartje samen met ons</a><span class="ico-list"><a class="samenlink" href="javascript:;" data-title="Stuur een los labeltje aan je kaartje"><img src="/img/card-detail/ico-drieluik.png"></a><a class="samenlink" href="javascript:;" data-title="Maak samen met ons een drieluik kaartje"><img src="/img/card-detail/ico-met-flap.png"></a><a class="samenlink" href="javascript:;" data-title="Voeg een echt strikje toe"><img src="/img/card-detail/ico-strikje.png"></a><a class="samenlink" href="javascript:;" data-title="Maak samen met ons een kaart met flap"><img src="/img/card-detail/ico-labeltje.png"></a><a href="/verzendservice" data-title="Stuur adreslabels op maat"><img src="/img/card-detail/ico-adreslabels.png"></a></span></div>');
        } else {
            $body_design.find('.details').after('<form action="/samen" method="get" id="samenfrm"><input type="hidden" name="url" id="url" value="' + loc + '"><input type="hidden" id="src1" name="src1" value="' + src1 + '"><input type="hidden" id="src2" name="src2" value="' + src2 + '"></form><div class="extra"><span>Wil je wat extra&rsquo;s?</span><a href="javascript:;" id="samenbutton" rel="nofollow">Ontwerp dit kaartje samen met ons</a><span class="ico-list"><a class="samenlink" href="javascript:;" data-title="Stuur een los labeltje aan je kaartje"><img src="/img/card-detail/ico-drieluik.png"></a><a class="samenlink" href="javascript:;" data-title="Maak samen met ons een drieluik kaartje"><img src="/img/card-detail/ico-met-flap.png"></a><a class="samenlink" href="javascript:;" data-title="Voeg een echt strikje toe"><img src="/img/card-detail/ico-strikje.png"></a><a class="samenlink" href="javascript:;" data-title="Maak samen met ons een kaart met flap"><img src="/img/card-detail/ico-labeltje.png"></a><a href="/verzendservice" data-title="Stuur adreslabels op maat"><img src="/img/card-detail/ico-adreslabels.png"></a></span></div>');
        }

        $("#samenbutton, a.samenlink").on("click", function() {
            return OnButton();
        });

    } else {
        $(".priceInfoTd .priceHover table tbody tr:first-child").remove();
        $("#body_design a.save_user_design").remove();
    }

    function OnButton() {
        document.getElementById('samenfrm').submit(); // Submit the page
        return true;
    }

    //tooltips samen
    jQuery("#body_design .extra .ico-list a").hover(function() {
        var $this = jQuery(this);
        var title = $this.attr("data-title");
        $this.parent().append("<i class='ico'></i><span class='tooltip'><span>" + title + "</span></span>");
        var tooltip = $this.parent().find('.tooltip');
        tooltip.css({ 'position': 'absolute', 'top': '63px', 'z-index': '9999' });
        var l = $this.offset().left - $this.parents(".extra").offset().left;
        var w = tooltip.outerWidth();
        var wico = $this.width();
        var half_wico = wico / 2;
        var left = l - w / 2;
        $this.parent().find('i').css('left', l + half_wico - 27 + 'px');
        if (l > 124) {
            tooltip.css({ 'right': '-10px', 'text-align': 'right' });
        } else if (l < 124) {
            if ($this.parent().find('.tooltip').find('span').outerWidth() > $this.parents('.extra').outerWidth()) {
                var ll = $this.parent().find('.tooltip').find('span').outerWidth() - $this.parents('.extra').outerWidth();
                tooltip.css('left', -20 - ll + 'px');
            } else {
                tooltip.css('left', '-20px');
            }
        } else if (l = 124) {
            tooltip.css({ 'left': left + 'px', 'text-align': 'center', 'width': '239px' });
        }
    }, function() {
        var $this = jQuery(this);
        var tooltip = $this.parent().find('.tooltip');
        $this.parent().find('i').remove();
        tooltip.remove();
    });


    // voor accord op design pagina, gejat van hipdesign
    if ($('#body_design').length) {
        $('div.detAccord .innerHtml').hide();
        $('div.detAccord.first .innerHtml').hide();
    } else {
        $('div.detAccord .innerHtml').hide();
        $('div.detAccord:eq(2) .innerHtml').show();
        $('div.detAccord:eq(2) .detAccordTitle').addClass('active');
    }

    //
    $('#card_previews a img').hover(function() {
        $(this).animate({ opacity: 0.7 }, 500);
    }, function() {
        $(this).animate({ opacity: 1 }, 500);
    });


    if ($('#filter_menu').length > 0) {



        /*jQuery('#taggroupul-voor').before(jQuery('#taggroupul-cardformat'));
         jQuery('#taggroupul-voor').before(jQuery('#taggroupul-cardorientation'));
         jQuery('#taggroupul-voor').before(jQuery('#taggroupul-kleur'));*/
        if ($('#filter_menu #taggroupul-kleur').length && $('#filter_menu .tag-group-ul:eq(3)').length) {
            $('#filter_menu #taggroupul-kleur').after('<div class="meer"><a href="javascript:;"><h3>Meer filters</h3></a></div>');
        } else {
            $('#filter_menu .tag-group-ul:eq(3)').before('<div class="meer"><a href="javascript:;"><h3>Meer filters</h3></a></div>');
        }

        $('.meer').nextAll('.tag-group-ul').hide();

        $('.meer').click(function() {
            $(this).find('a').toggleClass('open');
            $(this).nextAll('.tag-group-ul').fadeToggle();
        });

        $('#filter_menu ul li h3').click(function() {
            var $this = $(this);
            if ($this.hasClass('open')) {
                $this.next('ul').slideDown();
                $this.removeClass('open');
            } else {
                $this.next('ul').slideUp();
                $this.addClass('open');
            }
        });
    }
    var filterCardElm = $('#filter_cards').insertAfter('#filterCardInsert');


    $('#sidebar li').each(function() {
        var hre = $(this).find('a').attr('href');
        if (hre == pathname) {
            $(this).find('a').addClass('active');
        }
    });

    if (jQuery(".zoom").length) {
        jQuery(".zoom").click(function(e) {
            e.preventDefault();
            var html = '';
            var src2 = jQuery(this).parent().prev().attr('src');
            var src1 = jQuery(this).parent().prev().prev().attr('src');
            html += '<img class="sm-5" src="' + src1 + '" alt="">';
            html += '<img class="sm-7" src="' + src2 + '" alt="">';
            jQuery('body').append('<div id="bg-lightbox"></div>').show('slow');
            jQuery('body').append('<div id="lightbox" class="cols"><script>jQuery(document).ready(function(){jQuery("#bg-lightbox").click(function(){jQuery(this).hide("slow");jQuery(this).remove();jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});jQuery("#lightbox .close").click(function(){jQuery("#bg-lightbox").hide("slow");jQuery("#bg-lightbox").remove();jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});});</script><span class="close">Close</span>' + html + '</div>').show('slow');
        });
    }


    jQuery('#body_account #pagebody .cols-multi > div:nth-child(3)').remove();


    $('.popup-modal-foto').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#username'
    });
    $('.popup-modal-proefdruk').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#username'
    });

    // My code
    var $hideSidebar = $('#t-hide-sidebar'),
        $body = $('body'),
        $win = $(window),
        masonryOptions = {
            itemSelector: '.block-item'
        },
        timeoutGrid;

    if ($hideSidebar.hasClass('hide')) {
        $body.addClass('hide-sidebar');
    }

    var $grid;

    $('.grid-masonry').imagesLoaded(function() {
        $grid = $('.grid-masonry').masonry(masonryOptions);
    });

    $win.on('resize.grid-masonry', function() {
        clearTimeout(timeoutGrid);
        timeoutGrid = setTimeout(function() {
            $grid.masonry('destroy'); // destroy
            $grid.masonry(masonryOptions); // re-initialize
        }, 300);
    });

    var hasZelf = $('body').has("#has-zelf");
    if (hasZelf = true) {
        $('body').addClass('zelf');
    }

    if ($('#sidebar').length) {
        $('body').addClass("category");

    }

    if ($('.basket-m').length) {
        $('.basket-m').click(function() {
            $('.header-login-m').hide();
            $('.header-m').toggle('slow');
        })
    }

    if ($('.login-m').length) {
        $('.login-m').click(function() {
            $('.header-m').hide();
            $('.header-login-m').toggle('slow');
        })
    }

    $(window).load(function() {


        if ($('.header-m').length) {
            if ($('#navbar-basket').text().trim() == '0 items') {
                $('.header-m .des').text('Je winkelmandje is leeg');
            } else if ($('#navbar-basket').text().trim().substring(0, 2) == '1 ') {
                $('.header-m .des').text('1 item in je winkelmandje');
            } else {
                $('.header-m .des').text($('#navbar-basket').text() + 'in je winkelmandje');
            }
        }
        if ($('#template-sidebar').length) {
            if ($('#template-sidebar').hasClass('full')) {
                $('#categories').addClass('hide');
            }
        }
    });

    if ($('#template-sidebar').length) {
        if ($('#template-sidebar').hasClass('full')) {
            $('#categories').insertAfter($('#pagecontent'));
            $('#pagecontent').addClass('w-100');
        }
    }

    if ($('.kleur')) {
        $('.kleur .tag-li').each(function() {
            var $hoverText = $(this).find('label').clone().children().remove().end().text().trim();
            $(this).find('label').attr("title", $hoverText);
        });
    }


})(jQuery);
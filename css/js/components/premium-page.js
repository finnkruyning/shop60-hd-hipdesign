(function ($) {

    $(document).ready(function(){

        if ($('#premium-page').length > 0){

            $(window).scroll(function(){

                // ON SCROLL PLACE MENU
                stickIt();

            });

            stickIt();

        }

        // CLICK SCROLL TO LINK, USE DATA ATRIBUTE AS ID
        $('#premium-page').on('click', '.js-scroll-to', function(){
            
            var that = this;

            goToByScroll($($(that).data('scroll-to-link')), 100);
            
            // MAINTAIN BOTH ORIGINAL AND CLONED MENUS
            $('[data-scroll-to-link="'+$(that).data('scroll-to-link')+'"]').siblings().removeClass('active');
            $('[data-scroll-to-link="'+$(that).data('scroll-to-link')+'"]').addClass('active');

            // REMOVE ACTIVE STATE AFTER TIMEOUT
            setTimeout(function(){
                $('[data-scroll-to-link="'+$(that).data('scroll-to-link')+'"]').removeClass('active');
            }, 2500);

        });
  

        // STICK PREMIUM MENU TO TOP ON SCROLL
        // Create a clone of the menu, right next to original.
        $('.premium-page-header__menu').addClass('original').clone().insertAfter('.premium-page-header__menu').addClass('cloned').removeClass('original');
        
        // ON LOAD CHECK FOR PLACEMENT

        function goToByScroll(elm, offset) {
            $('html,body').animate({
                scrollTop: (elm.offset().top - offset)+"px"},
            'slow');
        }

        // CHECK WHICH MENU TO SHOW, FIXED OR INLINE
        function stickIt() {
            if($('.premium-page-header__menu.original').length > 0){
                var orgElementPos = $('.premium-page-header__menu.original').offset();
                var orgElementTop = orgElementPos.top;
                var headerOffset = 40;               

                if ($(window).scrollTop() + headerOffset >= (orgElementTop)) {
                    
                    // scrolled past the original position; now only show the cloned, sticky element.

                    // Cloned element should always have same left position and width as original element.     
                    orgElement = $('.original');
                    coordsOrgElement = orgElement.offset();
                    leftOrgElement = coordsOrgElement.left;  
                    widthOrgElement = orgElement.css('width');
                    $('.cloned').css('left',leftOrgElement+'px').css('width',widthOrgElement).addClass('show');

                } else {

                    // not scrolled past the menu; only show the original menu.
                    $('.cloned').removeClass('show');
                }
  
            }
        }
    });
})(jQuery);

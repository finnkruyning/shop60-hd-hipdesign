(function($) {

    var $navbar = $('.navbar');
    var $navbarlist = $navbar.find('.dropdown-content');
    $navbarlist.hide();
    var listCollapseLimit = 30;

    function moveLogo() {
        if ($(window).width() > 992) {
            var croll = $(window).scrollTop();
            if (croll > 80) {
                $navbar.find('.logosmall').fadeIn();
            } else {
                $navbar.find('.logosmall').fadeOut();
            }
        }

    }

    function navbarAccordion(e) {
        console.log(e);
        e.preventDefault();
        var _self = $(this);
        var siblings = $(this).parent().siblings('.open');
        siblings.removeClass('open').find('.dropdown-content').slideUp(400);
        var target = _self.parent().find('.dropdown-content');
        _self.parent().toggleClass('open');
        target.slideToggle(400);
    }

    $navbarlist.each(function() {
        var _self = $(this);

        if (_self.hasClass('list-collapse')) {
            _self.find('>li:gt(' + (listCollapseLimit - 1) + ')').hide();
            _self.append('<li class="list-toggle"><a class="list-toggle-text" href="#">Meer resultaten</a></li>');

            if (_self.find('>li:not(:last-child)').length > listCollapseLimit) {
                _self.find('.list-toggle').show();
            } else {
                _self.find('.list-toggle').hide();
            }
        }

    });

    $navbarlist.find('.list-toggle').click(function(e) {
        e.preventDefault();
        var _self = $(this);
        var lists = _self.closest($navbarlist).find('>li:gt(' + (listCollapseLimit - 1) + ')');

        if (_self.hasClass('list-toggle-show')) {
            _self.find('.list-toggle-text').html('Meer resultaten');
            _self.removeClass('list-toggle-show');
            lists.hide().last().show();

        } else {
            _self.find('.list-toggle-text').html('Minder resultaten');
            _self.addClass('list-toggle-show');
            lists.show().last().show();

        }
    });

    $navbar.find('.navbar-search-field').hide();



    $navbar.find('.navbar-hamburger').on('click', function(e) {

        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($navbar).find('.collapse');
        $('.navbar-hamburger').toggleClass('open');
        target.slideToggle(400, function() {
            if (target.is(":visible")) {
                $('.navbar-hamburger').addClass('open');
                $('html, body').css('overflowY', 'hidden');
            } else {
                $('.navbar-hamburger').removeClass('open');
                $('html, body').css('overflowY', 'auto');
            }
        });
    });

    $navbar.find('.dropdown > a').on('click', function(e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest('.dropdown').find('.dropdown-content');
        target.toggle();
    });

    $navbar.find('.dropdown > a').on('mouseenter', function(e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest('.dropdown').find('.dropdown-content');
        target.show();
    });

    $(document).click(function() {
        $navbar.find('.navbar-search-field').hide();
    });

    moveLogo();

    $(window).scroll(function() {
        moveLogo();
    });

    // if screen is less then
    if ($(window).width() < 992) {
        $navbar.find('.middlebar-menu-right > li > a').off();
        $navbar.find('.middlebar-menu-right > li > a').on('click', navbarAccordion);
        var url = window.location.pathname;
        console.log(url);
        // OPEN MOBILE MENU
        if (typeof(url) != 'undefined') {
            var urlPart = url.split("/")[1];
            if(urlPart == 'trouwkaarten'){
                if (url.search('save-the-date') != -1){
                    urlPart = 'trouwkaarten/save-the-date'
                }
            }
            console.log(urlPart);
            var thisMenuItem = $('.middlebar-menu-right').find('a[href^="/' + urlPart + '"]');
            if (typeof(thisMenuItem) != 'undefined') {
                thisMenuItem.first().siblings('ul').css({ 'display': 'flex' });
                thisMenuItem.first().parent('li').addClass('open');
            }

        }
    } else {
        $('html, body').css('overflowY', 'auto');
        $navbar.find('.middlebar-menu-right > li > a').off();
        $navbarlist.hide();
    }

    $(window).on('resize', function() {
        if ($(window).width() < 992) {
            $navbar.find('.middlebar-menu-right > li > a').off();
            $navbar.find('.middlebar-menu-right > li > a').on('click', navbarAccordion);
        } else {
            $('html, body').css('overflowY', 'auto');
            $navbar.find('.middlebar-menu-right > li > a').off();
            $navbarlist.hide();
        }
    });



})(jQuery);
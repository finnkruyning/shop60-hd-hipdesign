(function($) {



    var $accordion = $('.accordion');

    $accordion.each(function () {
        var _self = $(this);
        var openAccordion = _self.find('.accordion-button');
        var accordionBody = _self.find('.accordion-body');

        $('<i class="icon"></i>').appendTo(openAccordion);

        if (accordionBody.hasClass('show')) {
            _self.find('accordion-button').addClass('active');
            openAccordion.find('.icon').addClass('icon-chevron-down');
        } else {
            _self.find('accordion-button').removeClass('active');
            openAccordion.find('.icon').addClass('icon-chevron-right');
        }

    });

    $accordion.find('.accordion-button').on('click', function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($accordion).find('.accordion-body');
        var icon = _self.find('.icon');

        target.slideToggle(400, function() {
            target.toggleClass('show');
            
            if(target.is(":visible")){
                _self.addClass('active');
                icon.removeClass('icon-chevron-right').addClass('icon-chevron-down');
            } else {
                _self.removeClass('active');
                icon.removeClass('icon-chevron-down').addClass('icon-chevron-right');
            }
        });        
    });


})(jQuery);

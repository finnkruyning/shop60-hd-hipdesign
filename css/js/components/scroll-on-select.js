
/** SCROLL TO ID ON PAGE FROM SELECTBOX -----------------------------------**/

/*

Create selectbox with class 'js-scroll-to-select' and options value '#some_id'.

*/


(function($) {

    var $idSelector = $('select.js-scroll-on-select');

    $($idSelector).on('change', function(){
        var theOffset = $(this).data('scroll-on-click-offset') ? $(this).data('scroll-on-click-offset') : 50;
        var theDiv = $('#'+$(this).val());
        if (theDiv.length > 0){
            var theDivTop = theDiv.offset().top;
            $('html, body').animate({
                scrollTop: theDivTop - theOffset
            });
        }
    });

})(jQuery);


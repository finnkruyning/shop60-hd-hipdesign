

(function($) {

    // PUT HIGHLIGHT MENUS FROM MENU CONTENT IN OBJECT

    $(document).ready(function() {

        var highlightMenus = $('.js-highlight-menus');

        /** PLACE MOBILE HIGHLIGHT MENU FROM MENU TEMPLATE BASED ON URL -----------------------------**/



        if (typeof url == 'undefined' || typeof url == 'object' || url == '') {
            console.log('no url');
            var urlParts = [];
            if (typeof main_cat != 'undefined' && main_cat != '') {
            console.log('main_cat');
                urlParts.push(main_cat);
            }
            if (typeof the_main_category != 'undefined' && the_main_category != '') {
            console.log('the_main_category');
                urlParts.push(the_main_category);
            }
            if (typeof the_category != 'undefined' && the_category != '') {
            console.log('the_category');
                urlParts.push(the_category);
            }

        } else {
            var searchUrl = url;
            var urlParts = searchUrl.split('/');
        }

        var searchId = 'highlight';

        if (urlParts.length > 0 && urlParts[0] != '') {
            for (var i = 0; i < urlParts.length; i++) {
                var newSearchId = searchId + '_' + urlParts[i];
                if ($(highlightMenus).find("div[id^='" + newSearchId + "']").length > 0) {
                    searchId = newSearchId;
                }
            }
        }

        var highlightMenu = $(highlightMenus).find('#' + searchId).html();
        if (highlightMenu !== undefined && highlightMenu.length) {
            $('.mobile-nav__highlight-items').fadeOut(function() {
                $('.mobile-nav__highlight-items').html(highlightMenu).fadeIn();
            });
        }

    });


})(jQuery);

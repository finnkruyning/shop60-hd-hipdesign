
/** SCROLL TO ID ON PAGE FROM CLICK -----------------------------------**/

/*

Create div with class 'js-scroll-to-click' and list of a href value '#some_id'.

*/


(function($) {

    var $idSelector = $('.js-scroll-on-click');

    $($idSelector).on('click', 'a', function(e){
        e.preventDefault();
        var theOffset = $(this).data('scroll-on-click-offset') ? $(this).data('scroll-on-click-offset') : 50;
        var theDiv = $($(this).attr('href'));
        if (theDiv.length > 0){
            var theDivTop = theDiv.offset().top;
            $('html, body').animate({
                scrollTop: theDivTop - theOffset
            });
        }
    });

})(jQuery);


/** TODO:  IS THIS FILE STILL IN USE? ONLY LOADED IN hip_style_tmplt.html, OLD HTML **/



function placeHolder(element) {
    var standard_message = $(element).val();
    $(element).focus(
            function () {
                if ($(this).val() == standard_message)
                    $(this).val("");
            }
    );
    $(element).blur(
            function () {
                if ($(this).val() == "")
                    $(this).val(standard_message);
            }
    );
}

function goToByScroll(elm, px) {
    $('html,body').animate({
        scrollTop: elm.offset().top - px},
    'slow');
}

function OnButton() {
    document.getElementById('samenfrm').submit();             // Submit the page
    return true;
}



jQuery(document).ready(function () {

    if (jQuery('.trouwkaarten').length > 0) {
        jQuery('#drop-geboor').remove();
        if(jQuery('#body_trouwkaarten').length){
            jQuery('#drop-trouwkaarten').remove();
        }
    } else {
        jQuery('#drop-trouwkaarten').remove();
    }

    if (jQuery("#hidden").length > 0) {
        jQuery("body").addClass("temp-one");
    }
    if (jQuery(".image-menu").length > 0) {
        var $menu = jQuery(".image-menu").html();
        jQuery("#categories").append('<div id="image-menu">' + $menu + '</div>');
        jQuery(".image-menu").remove();
    }

    jQuery(".card-list li:last-child").addClass('last');
    jQuery(".block-list li:odd").addClass('right');

    var tophead = jQuery("#headermenu").html() + jQuery(".fixed").html();
    jQuery('#wrapper').before("<div class='tophead'><div class='topheadinner'>" + tophead + "</div></div>");

    var fnav = jQuery(".fnav-block").html();
    jQuery('body').append(fnav);


    jQuery('#headerlogin span').replaceWith('<span>Inloggen</span>');
    jQuery('#headerlogout span').replaceWith('<span>Uitloggen</span>');

    jQuery(".search-form input[type='text']").each(function (index, element) {
        placeHolder(jQuery(this));
    });

    jQuery('#card_previews a img').hover(function () {
        jQuery(this).animate({opacity: 0.7}, 500);
    }, function () {
        jQuery(this).animate({opacity: 1}, 500);
    });

    jQuery("#navInfo").hover(function () {
        jQuery('ul', this).stop(true, true).delay(200).slideDown(200);
    }, function () {
        jQuery('ul', this).stop(true, true).slideUp(200);
    });

    jQuery('#headerhelp').remove();

    jQuery("#navHulp").hover(function () {
        jQuery('ul', this).stop(true, true).delay(200).slideDown(200);
    }, function () {
        jQuery('ul', this).stop(true, true).slideUp(200);
    });


    var pathname = window.location.pathname;
    pathname_a = pathname.split("/");
    var l = pathname_a.length;

    moveLogo();

    jQuery(window).scroll(function () {
        moveLogo();
    });


    function moveLogo(){
        var croll = jQuery(window).scrollTop();
        if (croll > 10) {
            jQuery('.save-date').parents('div.tophead').addClass('bor');
            jQuery('.save-date').addClass('showlogo');
            jQuery('.save-date .logosmall').fadeIn();
            jQuery('#home').hide();
        } else {
            jQuery('.save-date').parents('div.tophead').removeClass('bor');
            jQuery('.save-date').removeClass('showlogo');
            jQuery('.save-date .logosmall').fadeOut();
            jQuery('#home').show();
        }
    }


    jQuery('#menu li').each(function () {
        var hre = jQuery(this).find('a').attr('href');
        if (hre == pathname) {
            jQuery(this).find('a').addClass('active');
        }
    });

    var src = jQuery('#body_main_category #pagecontent .main-img img').attr('src');
    if (src != null) {
        jQuery('#body_main_category #pagecontent h1').before('<img src="' + src + '" alt="" id="main-img">');
    }

    if (jQuery('#body_prijzen').length > 0) {
        var hash = location.hash;
        switch (hash) {
            case '#trouwkaarten':

                jQuery('#tabcontent .tabitem:eq(1)').show();
                jQuery('#tabs li:eq(1)').addClass('active');
                break;
            case '#uitnodigingskaarten':
                jQuery('#tabcontent .tabitem:eq(2)').show();
                jQuery('#tabs li:eq(2)').addClass('active');
                break;
            case '#rouwkaarten':
                jQuery('#tabcontent .tabitem:eq(3)').show();
                jQuery('#tabs li:eq(3)').addClass('active');
                break;
            case '#wenskaarten':
                jQuery('#tabcontent .tabitem:eq(4)').show();
                jQuery('#tabs li:eq(4)').addClass('active');
                break;
            default :
                jQuery('#tabcontent .tabitem:eq(0)').show();
                jQuery('#tabs li:eq(0)').addClass('active');
                break;
        }
    }
    if (jQuery('#body_enveloppen-info').length > 0) {
        var hash = location.hash;
        switch (hash) {
            case '#trouwkaarten':
                jQuery('#tabcontent .tabitem:eq(1)').show();
                jQuery('#tabs li:eq(1)').addClass('active');
                break;
            case '#uitnodigingskaarten':
                jQuery('#tabcontent .tabitem:eq(2)').show();
                jQuery('#tabs li:eq(2)').addClass('active');
                break;
            case '#rouwkaarten':
                jQuery('#tabcontent .tabitem:eq(3)').show();
                jQuery('#tabs li:eq(3)').addClass('active');
                break;
            case '#wenskaarten':
                jQuery('#tabcontent .tabitem:eq(4)').show();
                jQuery('#tabs li:eq(4)').addClass('active');
                break;
            default :
                jQuery('#tabcontent .tabitem:eq(0)').show();
                jQuery('#tabs li:eq(0)').addClass('active');
                break;
        }
    }
    jQuery("#tabs li").click(function () {
        if (jQuery(this).hasClass("active")) {
            return false;
        }
        jQuery("#tabs li").removeClass("active");
        jQuery(this).addClass("active");
        var index = jQuery("#tabs li").index(jQuery(this));
        jQuery("#tabcontent .tabitem").hide(0);
        jQuery("#tabcontent .tabitem").eq(index).fadeIn(300);

        return false;
    })

    jQuery('#body_design #design').append('<div id="card-detail-link" style="display:none"><a href="javascript:;" onclick="history.go(-1);return false;"><span>&lt;&lt;</span> <span class="txt">Ga terug</span></a></div>');
    if (pathname_a[1] != 'wenskaarten') {
        var url = location.href;
        var src1 = jQuery('#voor_preview img').attr('src');
        var src2 = jQuery('#binnen_preview img').attr('src');
        jQuery('#body_design #design').append('<form action="/samen" method="get" id="samenfrm"><input type="hidden" name="url" id="url" value="' + url + '"><input type="hidden" id="src1" name="src1" value="' + src1 + '"><input type="hidden" id="src2" name="src2" value="' + src2 + '"></form><div class="extra"><span>Wil je wat extra&rsquo;s?</span><a href="javascript:;" onclick="return OnButton();" rel="nofollow">Ontwerp dit kaartje samen met ons</a><span class="ico-list"><a href="javascript:;" data-title="Stuur een los labeltje aan je kaartje"><img src="/img/card-detail/ico-drieluik.png"></a><a href="javascript:;" data-title="Maak samen met ons een drieluik kaartje"><img src="/img/card-detail/ico-met-flap.png"></a><a href="javascript:;" data-title="Voeg een echt strikje toe"><img src="/img/card-detail/ico-strikje.png"></a><a href="javascript:;" data-title="Maak samen met ons een kaart met flap"><img src="/img/card-detail/ico-labeltje.png"></a><a href="javascript:;" data-title="Stuur adreslabels op maat"><img src="/img/card-detail/ico-adreslabels.png"></a></span></div>');
    }

    if (jQuery("#body_design").length > 0) {
        var href = jQuery("a#choose_preview").attr("href");
        jQuery("#voor_preview img").wrap("<a href='" + href + "'>");
        jQuery("#binnen_preview img").wrap("<a href='" + href + "'>");
    }

    if (jQuery(".zoom").length) {
        jQuery(".zoom").click(function (e) {
            e.preventDefault();
            var html = '';
            var src2 = jQuery(this).parent().prev().attr('src');
            var src1 = jQuery(this).parent().prev().prev().attr('src');
            html += '<img src="' + src1 + '" alt="">';
            html += '<img src="' + src2 + '" alt="">';
            jQuery('body').append('<div id="bg-lightbox"></div>').show('slow');
            jQuery('body').append('<div id="lightbox"><script>jQuery(document).ready(function(){jQuery("#bg-lightbox").click(function(){jQuery(this).hide("slow");jQuery(this).remove();jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});jQuery("#lightbox .close").click(function(){jQuery("#bg-lightbox").hide("slow");jQuery("#bg-lightbox").remove();jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});});</script><span class="close">Close</span>' + html + '</div>').show('slow');
        });
    }

    jQuery("#design .extra .ico-list a").hover(function () {
        var $this = jQuery(this);
        var title = $this.attr("data-title");
        $this.parent().append("<i class='ico'></i><span class='tooltip'><span>" + title + "</span></span>");
        var tooltip = $this.parent().find('.tooltip');
        tooltip.css({'position': 'absolute', 'top': '63px'});
        var l = $this.offset().left - $this.parents(".extra").offset().left;
        var w = tooltip.outerWidth();
        var wico = $this.width();
        var half_wico = wico / 2;
        var left = l - w / 2;
        $this.parent().find('i').css('left', l + half_wico - 27 + 'px');
        if (l > 124) {
            tooltip.css({'right': '-10px', 'text-align': 'right'});
        } else if (l < 124) {
            if ($this.parent().find('.tooltip').find('span').outerWidth() > $this.parents('.extra').outerWidth()) {
                var ll = $this.parent().find('.tooltip').find('span').outerWidth() - $this.parents('.extra').outerWidth();
                tooltip.css('left', -20 - ll + 'px');
            } else {
                tooltip.css('left', '-20px');
            }
        } else if (l = 124) {
            tooltip.css({'left': left + 'px', 'text-align': 'center', 'width': '239px'});
        }
    }, function () {
        var $this = jQuery(this);
        var tooltip = $this.parent().find('.tooltip');
        $this.parent().find('i').remove();
        tooltip.remove();
    });

    jQuery('#crumbs').next("h1:first").addClass('top-title-block');

    if (jQuery('.menu-prijzen').length > 0) {
        jQuery('.dropdown').click(function (e) {
            e.preventDefault();
            if (jQuery(this).hasClass("open")) {
                jQuery(this).parent().next().slideUp();
                jQuery(this).removeClass('open');
            } else {
                jQuery(this).parent().next().slideDown();
                jQuery(this).addClass('open');
            }
        });

        jQuery('.dropdown-1').click(function (e) {
            e.preventDefault();
            if (jQuery(this).hasClass("open")) {
                jQuery(this).parent().parent().next().slideUp();
                jQuery(this).removeClass('open');
            } else {
                jQuery(this).parent().parent().next().slideDown();
                jQuery(this).addClass('open');
            }
        });
        /*jQuery('.dropdowna').toggle(function(){
         jQuery(this).parent().next().slideDown();
         jQuery(this).addClass('open');
         },function(){
         jQuery(this).parent().next().slideUp();
         jQuery(this).removeClass('open');
         });*/
    }

    jQuery('.menu-prijzen ul li').each(function () {
        var h = jQuery(this).find('a').attr('href');
        if (pathname == h) {
            jQuery(this).addClass('active');
            jQuery(this).parent('ul').show();
        }
    });

    jQuery('.menu-prijzen ul li a').click(function () {
        var h = jQuery(this).attr('href');
        var h_a = h.split('#');

        if (h_a.length > 1) {
            jQuery('.menu-prijzen ul li a').removeClass('subactive');
            jQuery(this).addClass('subactive');
            var id = h_a[1];
            var $id = jQuery('#' + id);
            var inner = $id.find(".itemcontent");

            if ($id.find('h2 a').hasClass("open")) {
                $id.find('h2 a').removeClass("open");
                $id.find(".itemcontent").slideUp(300);
            } else {
                $(".item-prijzen .itemcontent").not(inner).hide();
                $(".item-prijzen h2 a").removeClass("open");
                $id.find('h2 a').addClass("open");
                $id.find(".itemcontent").slideDown(300);
                goToByScroll($id, 37);
            }
        }
    });


    var hash1 = location.hash;
    var hash_a = hash1.split('#');
    if (hash_a.length > 1) {
        $id = jQuery('#' + hash_a[1]);
        var inner = $id.find(".itemcontent");

        if ($id.find('h2 a').hasClass("open")) {
            $id.find('h2 a').removeClass("open");
            $id.find(".itemcontent").slideUp(300);
        } else {
            jQuery(".item-prijzen .itemcontent").not(inner).hide();
            jQuery(".item-prijzen h2 a").removeClass("open");
            $id.find('h2 a').addClass("open");
            $id.find(".itemcontent").slideDown(300);
        }
    }

    if (jQuery('.prijzen-wrap').length > 0) {
        jQuery(".item-prijzen:first-child .itemcontent").show();
        jQuery('.item-prijzen:first-child h2 a').addClass("open");

        jQuery(".item-prijzen h2 a").click(function (e) {
            e.preventDefault();
            var inner = jQuery(this).parent().parent().find(".itemcontent");
            if (jQuery(this).hasClass("open")) {
                jQuery(this).removeClass("open");
                jQuery(this).parent().parent().find(".itemcontent").slideUp(300);
            } else {
                jQuery(".item-prijzen .itemcontent").not(inner).hide();
                jQuery(".item-prijzen h2 a").removeClass("open");
                jQuery(this).addClass("open");
                jQuery(this).parent().parent().find(".itemcontent").slideDown(300);
            }
            jQuery('html, body').animate({
                scrollTop: (jQuery(this).position().top)
            }, 500);
        });
    }

    jQuery('#categories .mainmenu li ul li a').each(function () {
        if (pathname == jQuery(this).attr('href')) {
            jQuery(this).addClass('active');
        }
    });

    if (jQuery('#body_samen').length > 0) {
        jQuery('#body_samen').addClass('subpage');
    }

    //Reviews inladen
    $.ajax({
        type: "GET",
        url: "/css/xml/hipdesign-reviews.xml",
        dataType: "xml",
        success: function (xml) {
            var $reviews = shuffle($("review", xml));
            var maxReviews = 1;
            $reviews.filter(":lt(" + maxReviews + ")").each(function (index, q) {
                var description = $(q).find('description').text(),
                        url = $(q).find('url').text();
                $('.feedback').html('<p>' + description + '<span><a href="' + url + '" target="_blank"> via the Feedback Company</a></span></p>');
            });
        },
        error: function () {
            $('#pagecontent .feedback').html("An error occurred while processing XML file.");
        }
    });

    if ($('#sidebar-footer').length) {
        $('#sidebar-footer').appendTo('#categories');

    }
    if ($('#voor_preview').length) {
        $('#sidebar-footer').remove();
    }

    if (jQuery('#filter_menu').length > 0) {



        /*jQuery('#taggroupul-voor').before(jQuery('#taggroupul-cardformat'));
         jQuery('#taggroupul-voor').before(jQuery('#taggroupul-cardorientation'));
         jQuery('#taggroupul-voor').before(jQuery('#taggroupul-kleur'));*/
        jQuery('#filter_menu .tag-group-ul:eq(3)').before('<div class="meer"><a href="javascript:;">Meer filters</a></div>');
        jQuery('.meer').nextAll('.tag-group-ul').hide();

        jQuery('.meer').click(function () {
            jQuery(this).find('a').toggleClass('open');
            jQuery(this).nextAll('.tag-group-ul').fadeToggle();
        });

        jQuery('#filter_menu ul li h3').click(function () {
            var $this = jQuery(this);
            if ($this.hasClass('open')) {
                $this.next('ul').slideDown();
                $this.removeClass('open');
            } else {
                $this.next('ul').slideUp();
                $this.addClass('open');
            }
        });
    }
    var filterCardElm = $('#filter_cards').insertAfter('#filterCardInsert');

    $('.cat ul li:gt(3)').hide();
    $('.cat ul li').last().show();

    $('.meer-resultaten').click(function (e) {
        e.preventDefault();
//        $(this).text('Minder resultaten').removeClass('meer-resultaten').addClass('minder-resultaten');

        $('.cat ul li:gt(3)').toggle();
        $(this).text(function (i, text) {
            return text === "Minder resultaten" ? "Meer resultaten" : "Minder resultaten";
        })
        $('.cat ul li').last().show();

    });



});

function shuffle(o) {
    for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x)
        ;
    return o;
}

console.log($('#body_account #pagebody .cols-multi > div:nth-child(3)'));
$('#body_account #pagebody .cols-multi > div:nth-child(3)').remove();

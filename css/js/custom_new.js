$(document).ready(function () {
    if ($(window).width() > 992) {
        $(window).scroll(function () {
            if ($(document).scrollTop() > 40) {
                $('.navbar .topbar').css({'position': 'fixed', 'top': '0'});
            };
        });
    };

    var pathname = window.location.pathname;
    var pathname_a = pathname.split("/");

    if(pathname_a[1] != 'wenskaarten') {
        $('#body_design #card_order_info').append('<div class="extras">' +
            '<h3>Wil je wat extra&rsquo;s?</h3>' +
            '<a class="together" href="/samen" target="_blank" data-title="Maak samen met ons een kaart met flap">' +
            '<div class="ext-img"><img src="/img/card-detail/ico-labeltje.png"></div>' +
            '<div class="ext-text"><span class="ext-header">Ontwerp samen</span><br />' +
            '<span>Ontwerp een kaartje met ons.</span></div></a>' +
            '<a class="sticker" href="/verzendservice" target="_blank" data-title="Stuur een los labeltje aan je kaartje">' +
            '<div class="ext-img"><img src="/img/card-detail/ico-drieluik.png"></div>' +
            '<div class="ext-text"><span class="ext-header">Adres of sluitsticker</span><br />' +
            '<span>Voeg een adres- of sluitsticker toe.</span></div></a>' +
            '<a class="mailservice" href="/verzendservice" target="_blank" data-title="Stuur adreslabels op maat">' +
            '<div class="ext-img"><img src="/img/card-detail/ico-adreslabels.png"></div>' +
            '<div class="ext-text"><span class="ext-header">Verzend Service</span><br />' +
            '<span>Wij verzorgen de verzending.</span></div></a>'+
            '</div>');
    };

    if($('#body_design')){
        var header = $('.top-title-block');

        $('.crumb-mobile').append(header);
    };
    
});


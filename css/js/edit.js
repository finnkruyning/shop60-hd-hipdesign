function OnButton (){
    if (jQuery('#body_edit').length >0){
        typeof(design_id) !== "undefined" ? design_id = design_id : design_id = jQuery('#object_design_id').val();
        typeof(category_name) !== "undefined" ? category_name = category_name : category_name = jQuery('#object_category').val();
        typeof(main_category_name) !== "undefined" ? main_category_name = main_category_name : main_category_name = jQuery('#object_main_category').val();
        console.log(main_category_name);
		jQuery('form#samenfrm #name_samen').val(design_id);
		jQuery('form#samenfrm #cat').val(category_name);
		jQuery('form#samenfrm #main_cat').val(main_category_name);
	}
    document.getElementById('samenfrm').submit(); // Submit the page
    return true;
}

if (typeof main_category_name !== 'undefined') {
	var category = main_category_name;
} else {
	var category = '';
}

jQuery(document).ready(function(){

	switch (category) {
		case 'geboortekaartjes' : $('.logo-editor').find('img').attr('src','/img/logo-geboorte.png'); break;
		case 'trouwkaarten' : $('.logo-editor').find('img').attr('src','/img/logo-trouwen.png'); break;
		case 'uitnodiging-maken' : $('.logo-editor').find('img').attr('src','/img/logo-uitnodigingen.png'); break;
		case 'rouwkaarten' : $('.logo-editor').find('img').attr('src','/img/logo-rouw.png'); break;
		case 'wenskaarten' : $('.logo-editor').find('img').attr('src','/img/logo-wenskaarten.png'); break;
		case 'jubileumkaarten' : $('.logo-editor').find('img').attr('src', '/img/logo-jubileumkaarten.png'); break;
	}

	if(jQuery('#body_cadeau_details').length > 0){
		var href = jQuery('#step_forward').attr('href');
		var html = '<p class="maak clearfix"><a href=' + href + ' class="button">Kies dit product &gt;&gt;</a></p>';
		jQuery('#cadeau_description').append(html);
	}

	var tophead = jQuery("#headermenu").html() + jQuery(".fixed").html();
	jQuery('#wrapper').before("<div class='tophead'><div class='topheadinner'>" + tophead + "</div></div>");

	jQuery('#headerlogin span').replaceWith('<span>Login</span>');
	jQuery('#headerlogout span').replaceWith('<span>Logout</span>');

	jQuery('#headerhelp').remove();

	jQuery("#navHulp").hover(function(){
		jQuery('ul',this).stop(true, true).delay(200).slideDown(200);
	}, function() {
		jQuery('ul',this).stop(true, true).slideUp(200);
	});

	jQuery('#body_addresses .address table tr:last-child').find('span#new').parents('tr').addClass('row-last');
	jQuery('#body_addresses .address table tr.row-last td:eq(1)').remove();
	jQuery('#body_addresses .address table tr.row-last td:eq(1)').attr('colspan','3');

	jQuery('#body_addresses .address table tr td').each(function(){
		jQuery(this).find('span#abook').parents('table').addClass('form-addresses');
		var rowspan = jQuery(this).attr('colspan');
		if (rowspan == 4) {
			jQuery(this).parent().remove();
		}
	});

	jQuery('#body_address .address table tbody tr:eq(1)').remove();
	jQuery('#body_address .address table tbody tr:eq(1)').remove();

	jQuery('#body_address .address table td #address_last_name').parent().prev().addClass('pad');
	jQuery('#body_address .address table td #address_suffix').parent().prev().addClass('pad');
	jQuery('#body_address .address table td #address_city').parent().prev().addClass('pad');

	jQuery('#basket td div').each(function() {
	var style=jQuery(this).attr('style');
	if (style='float:right;') {
	jQuery(this).addClass('price');
	}
	jQuery('#nieuwsbrief').parent().parent().addClass('block-form');
	});

	jQuery("#basket tr:eq(0)").addClass('row-title');


	// place infoboxes center on mobile, based on info-hover parent.

    $('body').on('mouseenter', 'info-hover', function(){
        $(this).parent().addClass('info-hover-parent');
        $(this).parents('table').addClass('info-hover-parent');
    });





	//tijdelijke oplossing om editor weer werkend te krijgen
    //jQuery('link[href="/css/less.css"]').remove();

	getModals()

});
